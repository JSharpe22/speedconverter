package com.example.speedconverter;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import android.widget.Button;
import android.view.View.OnClickListener;

public class MainActivity extends Activity {

	private EditText text;
	private int status = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
		text = (EditText) findViewById(R.id.editText1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
	
		Button button = (Button) findViewById(R.id.button1);
		Button clearButton = (Button) findViewById(R.id.button2);
		button.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Toast.makeText(MainActivity.this, "Button Clicked", Toast.LENGTH_SHORT).show();
				calculate(view);
			}
		});
		
		clearButton.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				text.setText("");
				status = -1;
			}
		});

		return true;
	}

	public void calculate(View view) {

		RadioButton mileButton = (RadioButton) findViewById(R.id.radioButton1);
		RadioButton kmhButton = (RadioButton) findViewById(R.id.radioButton2);
		// if the text field is empty show the message “enter a valid number”
		if (text.getText().length() == 0) 
		{
			// Toast = focused floating view that will be shown over the main
			// application
			Toast.makeText(this, "enter a valid number", Toast.LENGTH_LONG)
			.show();
		} 
		else {
		//parse input Value from Text Field

			double inputValue = Double.parseDouble(text.getText().toString());
			double temp = inputValue;

			if (kmhButton.isChecked())
			{
				if(status == 0)
				{
					text.setText(String.valueOf(inputValue));
					Toast.makeText(this, "1", Toast.LENGTH_LONG)
					.show();
				}
				else
				{
					text.setText(String.valueOf(convertToKmh(temp)));
					Toast.makeText(this, "2", Toast.LENGTH_LONG)
					.show();
					status = 0;
				}
			}
			else if(mileButton.isChecked())
			{
				if(status == 1)
				{
					text.setText(String.valueOf(inputValue));
					Toast.makeText(this, "3", Toast.LENGTH_LONG)
					.show();
				}
				else
				{
					text.setText(String.valueOf(convertToMiles(temp)));
					Toast.makeText(this, "4", Toast.LENGTH_LONG)
					.show();
					status = 1;
				}
			}
		}
	}
	
	private double convertToMiles(double inputValue) {
		// convert km/h to miles
		return (inputValue * 1.609344);
		}

	private double convertToKmh(double inputValue) {
		// convert miles to km/h
		return (inputValue * 0.621372);
		}

}

